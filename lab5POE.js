function muestra() {
    document.getElementById("muestra").innerHTML = 
      "<li>Con JavaScript y HTML5 [y quiz&aacute;s CSS], desarrolla una p&aacute;gina"+
          " para validar passwords. La p&aacute;gina debe tener una forma con 2"+
          " campos, el campo de password, y el campo de verificar"+
          " password. Utiliza al m&aacute;ximo tu creatividad e ingenier&iacute;a para"+
          " que la p&aacute;gina sea un validador de passwords de estado del"+
          " arte, con la mejor experiencia para el usuario.</li>"+
        "<li>Con JavaScript y HTML5 [y quizás CSS], desarrolla una p&aacute;gina"+
          " para vender 3 productos de tu inter&eacute;s, con los precios y"+
          " promociones a tu gusto. La p&aacute;gina debe permitir al usuario"+
          " escoger la cantidad de unidades de cada producto, y debe"+
          " mostrar el precio total, el IVA que se est&aacute; cargando, y toda"+
          " la informaci&oacute;n que consideres pertinente para que la"+
          " experiencia del usuario sea la mejor. La p&aacute;gina debe validar"+
          " los rangos de las unidades de cada producto.</li>"+
        "<li>Con JavaScript y HTML5 y CSS, desarrolla una p&aacute;gina con"+
          " alguna tem&aacute;tica o problema de tu inter&eacute;s. La p&aacute;gina debe"+
          " contener una forma y debes realizar las validaciones necesarias."+
          " Despu&eacute;s de validar la forma, la p&aacute;gina debe desplegar"+
          " informaci&oacute;n relacionada con los datos introducidos en la forma,"+
          " es decir la soluci&oacute;n del problema o informaci&oacute;n respectiva a"+
          " los datos introducidos.</li>"+
    "";
  }